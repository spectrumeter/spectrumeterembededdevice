# Spectrumeter - Smart Meter Data Extraction and Transmission

#### Author: Michael Rumpelnig


## Overview
This repository contains the source code for an ESP32-based application designed to read data from a smart meter via UART, process MBUS frames, extract relevant values, and transmit the data to a server using Wi-Fi. The code is organized into three main components:

Frame Data Extractor: Parses and extracts information from MBUS frames received from the smart meter.

Frame Processor: Decrypts and processes MBUS frames, extracting the necessary data for further analysis.

Wi-Fi HTTP Client: Establishes a Wi-Fi connection and sends the processed data to a server using HTTP requests.

## Components
### 1. Frame Data Extractor
File: FrameDataExtractor.h
Description: Implements functions for extracting specific values from the raw data obtained from the smart meter.

Functions:

find_in_mem: Searches for a specified byte sequence in memory.<br>
extract_values_from_data: Parses MBUS frames, extracts relevant data, and creates a JSON string.

### 2. Frame Processor
File: FrameProcessor.h
Description: Decrypts and processes MBUS frames to obtain meaningful data.

Functions:

process_mbus_frames: Decrypts and processes MBUS frames, returning the length of the extracted data.

### 3. Wi-Fi HTTP Client
Files: WifiHttpClient.h and WifiHttpClient.c
Description: Manages the Wi-Fi connection and sends HTTP requests to a server.

Functions:

wifi_init_sta: Initializes the Wi-Fi connection in station mode.<br>
send_data_to_server: Sends data to a specified server using HTTP requests.

## Main Application

File: main.c
Description: Combines the Frame Data Extractor, Frame Processor, and Wi-Fi HTTP Client to read data from the smart meter, process MBUS frames, extract relevant values, and transmit the data to a server.

Functions:

read_smartmeter_data: Main task that continuously reads MBUS frames, processes data, and sends it to the server.

## Initialization:

Configures UART for communication with the smart meter.
Initializes NVS for non-volatile storage.
Initializes Wi-Fi in station mode.

## Setup and Usage

### Hardware Setup:

Connect the ESP32 UART pins to the corresponding smart meter UART pins.
Configure Wi-Fi:

Update the Wi-Fi credentials in WifiHttpClient.c to match your network.
Server Configuration:

Modify the server details (IP address, port, API endpoint) in WifiHttpClient.c according to your server setup.
Build and Flash:

Build the project using the ESP-IDF framework.
Flash the compiled binary to your ESP32 device.

### Monitor Output:

Use a serial monitor to view the debug output from the ESP32.
Observing Results:

The application continuously reads MBUS frames, processes the data, and sends it to the specified server.
Feel free to customize the code and configurations based on your specific smart meter and server requirements. If you encounter any issues or have additional requirements, refer to the ESP-IDF documentation for further assistance.
