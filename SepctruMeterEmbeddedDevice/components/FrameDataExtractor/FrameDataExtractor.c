// Include necessary libraries
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "FrameDataExtractor.h"
#include "cJSON.h"
#include "esp_log.h"

// Define a tag for logging purposes
#define DATA_EXTRACTOR_TAG "Data Extractor"

// Function to find a pattern in memory
uint8_t * find_in_mem(uint8_t * haystack, uint8_t * needle, size_t haystack_size, size_t needle_size){
    for(size_t i = 0; i < haystack_size-needle_size; i++){
        size_t sum = 0;
        for(size_t j = 0; j<needle_size;j++){
            sum += *(haystack+i+j)==*(needle+j);
        }
        if(sum==needle_size){
            return haystack+i;
        }
    }
    return NULL;
}

// Function to extract values from raw data and create a JSON string
void extract_values_from_data(uint8_t * data, size_t data_len, char *json_data){
	ESP_LOGI(DATA_EXTRACTOR_TAG, "Extracting data and creating json string.");

	// Create a JSON object
	cJSON *json_object = cJSON_CreateObject();
	if (json_object == NULL) {
		printf("Failed to create JSON object!\n");
	    return;
	}

	// Define OBIS (Object Identification System) sizes
    uint8_t size = 6;

    // Define OBIS patterns for specific data points
    uint8_t timestamp[6] = {0x00, 0x00, 0x01, 0x00, 0x00, 0xff};
    uint8_t meternumber[6] = {0x00, 0x00, 0x60, 0x01, 0x00, 0xff};
    uint8_t voltage1[6] = {0x01, 0x00, 0x20, 0x07, 0x00, 0xff};
    uint8_t voltage2[6] = {0x01, 0x00, 0x34, 0x07, 0x00, 0xff};
    uint8_t voltage3[6] = {0x01, 0x00, 0x48, 0x07, 0x00, 0xff};
    uint8_t ampere1[6] = {0x01, 0x00, 0x1F, 0x07, 0x00, 0xff};
    uint8_t ampere2[6] = {0x01, 0x00, 0x33, 0x07, 0x00, 0xff};
    uint8_t ampere3[6] = {0x01, 0x00, 0x47, 0x07, 0x00, 0xff};
    uint8_t active_power_plus[6] = {0x01, 0x00, 0x01, 0x07, 0x00, 0xff};
    uint8_t active_power_minus[6] = {0x01, 0x00, 0x02, 0x07, 0x00, 0xff};
    uint8_t active_energy_plus[6] = {0x01, 0x00, 0x01, 0x08, 0x00, 0xff};
    uint8_t active_energy_minus[6] = {0x01, 0x00, 0x02, 0x08, 0x00, 0xff};
    uint8_t reactive_power_plus[6] = {0x01, 0x00, 0x01, 0x07, 0x00, 0xff};
    uint8_t reactive_power_minus[6] = {0x01, 0x00, 0x02, 0x07, 0x00, 0xff};

    // Extract data points for each OBIS pattern and add the values to a JSON object

    // Extract timestamp
    uint8_t * start = find_in_mem(data, timestamp, data_len, size);
    if(start!=NULL){
    	start += size+2;
        uint16_t year = *(start) << 8 | *(start+1);
        uint8_t month = *(start+2);
        uint8_t day = *(start+3);
        uint8_t hour = *(start+5);
        uint8_t minute = *(start+6);
        uint8_t second = *(start+7);
        char timestamp_buff[50] = {0};
        sprintf(timestamp_buff, "%02d-%02d-%02dT%02d:%02d:%02d",year, month, day, hour, minute, second);
        cJSON_AddItemToObject(json_object, "timestamp", cJSON_CreateString(timestamp_buff));
    }
    // Extract meternumber
    start = find_in_mem(data, meternumber, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint8_t meternumber_length = *(start);
        char meternumber[meternumber_length];
        memcpy(meternumber, start+1, meternumber_length);
        cJSON_AddItemToObject(json_object, "meternumber", cJSON_CreateString(meternumber));
    }
    // Extract voltage 1
    start = find_in_mem(data, voltage1, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t voltage_l1 = *(start)<< 8 | *(start+1);
        int8_t scale_factor = *(start+5);
        float scaled_voltage_1 = (float)voltage_l1 * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "voltage1", cJSON_CreateNumber(scaled_voltage_1));
    }
    // Extract voltage 2
    start = find_in_mem(data, voltage2, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t voltage_l2 = *(start)<< 8 | *(start+1);
        int8_t scale_factor = *(start+5);
        float scaled_voltage_2 = (float)voltage_l2 * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "voltage2", cJSON_CreateNumber(scaled_voltage_2));
    }
    // Extract voltage 3
    start = find_in_mem(data, voltage3, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t voltage_l3 = *(start)<< 8 | *(start+1);
        int8_t scale_factor = *(start+5);
        float scaled_voltage_3 = (float)voltage_l3 * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "voltage3", cJSON_CreateNumber(scaled_voltage_3));
    }
    // Extract ampere 1
    start = find_in_mem(data, ampere1, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t current_l1_buf = *(start)<< 8 | *(start+1);
        int8_t scale_factor = *(start+5);
        float current_l1 = (float)current_l1_buf/(float)scale_factor;
        float scaled_ampere_1 = (float)current_l1 * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "ampere1", cJSON_CreateNumber(scaled_ampere_1));
    }
    // Extract ampere 2
    start = find_in_mem(data, ampere2, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t current_l2_buf = *(start)<< 8 | *(start+1);
        int8_t scale_factor = *(start+5);
        float current_l2 = (float)current_l2_buf/(float)scale_factor;
        float scaled_ampere_2 = (float)current_l2 * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "ampere2", cJSON_CreateNumber(scaled_ampere_2));
    }
    // Extract ampere 3
    start = find_in_mem(data, ampere3, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t current_l3_buf = *(start)<< 8 | *(start+1);
        int8_t scale_factor = *(start+5);
        float current_l3 = (float)current_l3_buf/(float)scale_factor;
        float scaled_ampere_3 = (float)current_l3 * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "ampere3", cJSON_CreateNumber(scaled_ampere_3));
    }
    // Extract active power plus
    start = find_in_mem(data, active_power_plus, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint32_t active_power_plus_buf = *(start)<< 24 | *(start+1)<<16 | *(start+2)<< 8 | *(start+3);
        int8_t scale_factor = *(start+7);
        float scaled_active_power_plus = (float)active_power_plus_buf * (float)pow(10, scale_factor);
        char active_power_plus_buff[50] = {0};
        sprintf(active_power_plus_buff, "%f", scaled_active_power_plus);
        cJSON_AddItemToObject(json_object, "active_power_plus", cJSON_CreateNumber(scaled_active_power_plus));
    }
    // Extract active power minus
    start = find_in_mem(data, active_power_minus, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint32_t active_power_minus_buf = *(start)<< 24 | *(start+1)<<16 | *(start+2)<< 8 | *(start+3);
        int8_t scale_factor = *(start+7);
        float scaled_active_power_minus = (float)active_power_minus_buf * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "active_power_minus", cJSON_CreateNumber(scaled_active_power_minus));
    }
    // Extract active energy plus
    start = find_in_mem(data, active_energy_plus, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint32_t active_energy_plus_buf = *(start)<< 24 | *(start+1)<<16 | *(start+2)<< 8 | *(start+3);
        int8_t scale_factor = *(start+7);
        float scaled_active_energy_plus = (float)active_energy_plus_buf * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "active_energy_plus", cJSON_CreateNumber(scaled_active_energy_plus));
    }
    // Extract active energy minus
    start = find_in_mem(data, active_energy_minus, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint32_t active_energy_minus_buf = *(start)<< 24 | *(start+1)<<16 | *(start+2)<< 8 | *(start+3);
        int8_t scale_factor = *(start+7);
        float scaled_active_energy_minus = (float)active_energy_minus_buf * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "active_energy_minus", cJSON_CreateNumber(scaled_active_energy_minus));
    }
    // Extract reactive power plus
    start = find_in_mem(data, reactive_power_plus, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint16_t reactive_power_plus_buf = *(start)<< 24 | *(start+1)<<16 | *(start+2)<< 8 | *(start+3);
        int8_t scale_factor = *(start+7);
        float scaled_reactive_power_plus = (float)reactive_power_plus_buf * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "reactive_power_plus", cJSON_CreateNumber(scaled_reactive_power_plus));
    }
    // Extract reactive power minus
    start = find_in_mem(data, reactive_power_minus, data_len, size);
    if(start!=NULL){
    	start += size+1;
        uint32_t reactive_power_minus_buf = *(start)<< 24 | *(start+1)<<16 | *(start+2)<< 8 | *(start+3);
        int8_t scale_factor = *(start+7);
        float scaled_reactive_power_minus = (float)reactive_power_minus_buf * (float)pow(10, scale_factor);
        cJSON_AddItemToObject(json_object, "reactive_power_minus", cJSON_CreateNumber(scaled_reactive_power_minus));
    }

    // Convert JSON object to string
    char *json_string = cJSON_PrintUnformatted(json_object);
    char *json_string_formatted = cJSON_Print(json_object);

    // Print the formatted JSON object
    ESP_LOGI(DATA_EXTRACTOR_TAG, "%s", json_string_formatted);

    // Release JSON object
    cJSON_free(json_object);

    // Copy JSON string to output parameter
    strcpy(json_data, json_string);

    // Log the completion of the extraction of the data and the creation of the JSON string
    ESP_LOGI(DATA_EXTRACTOR_TAG, "Json string created.");
}
