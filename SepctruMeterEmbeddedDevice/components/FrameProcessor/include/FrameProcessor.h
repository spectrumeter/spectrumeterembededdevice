#include <stdio.h>

size_t processMbusFrames(uint8_t *first_frame, uint8_t *second_frame, uint8_t *decrypted_data);
