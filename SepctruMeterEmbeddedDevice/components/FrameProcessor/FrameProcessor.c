// Include necessary libraries
#include <string.h>
#include "FrameProcessor.h"
#include "mbedtls/gcm.h"
#include "esp_log.h"

// Define a tag for logging purposes
#define PROCESSOR_TAG "Frame Processor"

// Global variables for AES-GCM context and key
mbedtls_gcm_context aes;

// Smart Meter Key in HEX
char key[16] = {0x32,0x69,0x31,0x63,0x79,0x79,0x45,0x6C,0x59,0x37,0x34,0x44,0x73,0x6D,0x33,0x75};

// Function to process MBUS frames and decrypt data
size_t processMbusFrames(uint8_t *first_frame, uint8_t *second_frame, uint8_t *decrypted_data) {

	ESP_LOGI(PROCESSOR_TAG, "Starting processing of MBUS frames.");

	// Declare variables for the lengths of the data of the frames
	uint32_t length_data_first_frame = 0;
	uint32_t length_data_second_frame = 0;

	// Declare variable for title length from first frame
	uint32_t title_length = first_frame[10];

	// Extract APDU length (data length) first bit
	uint32_t APDU_length_indication_bit = first_frame[19];

	// Declare variables for the APDU length
	uint32_t APDU_length = 0;
	uint32_t APDU_length_offset = 0;

	// Determine the APDU length depending on indication bit and add the proper offset
	// APDU length can range from 1 to 3 bits
	if(APDU_length_indication_bit <= 0x80){
		APDU_length = first_frame[19];
		APDU_length_offset = 1;
	}else if(APDU_length_indication_bit == 0x81){
		APDU_length = first_frame[20];
		APDU_length_offset = 2;
	}else if(APDU_length_indication_bit == 0x82){
		APDU_length = first_frame[20] <<8| first_frame[21];
		APDU_length_offset = 3;
	}

	// Return if APDU length is zero
	if(APDU_length == 0){
		return 0;
	}

	// Extract title and frame counter
	uint8_t title[title_length];
	memcpy(title, first_frame + 11, title_length);
	uint32_t frame_counter;
	memcpy(&frame_counter, first_frame + 20 + APDU_length_offset, 4);

	// Create initialization vector (IV) for AES-GCM (title and frame counter combined)
	char iv[12];
	memcpy(iv, title, 8);
	memcpy(iv + 8, &frame_counter, 4);

	// Determine lengths of data in the first and second frames
	if(APDU_length >= 229){
		length_data_first_frame = 229;
		length_data_second_frame = APDU_length - 229;
	}else{
		length_data_first_frame = APDU_length;
	}

	// Extract encrypted data from frames
	uint8_t enc_data[APDU_length - 5];
	uint8_t dec_data[APDU_length - 5];
	memcpy(enc_data, first_frame + 24 + APDU_length_offset, length_data_first_frame - 2);
	memcpy(enc_data + length_data_first_frame - 2, second_frame + 9, length_data_second_frame - 3);

	// Initialize AES-GCM context and set key
	size_t data_read = 0;
	uint8_t tag[16];

	mbedtls_gcm_init(&aes);
	mbedtls_gcm_setkey(&aes, MBEDTLS_CIPHER_ID_AES , (const unsigned char*) key, 16 * 8);

	// Perform AES-GCM decryption
	mbedtls_gcm_starts(&aes, MBEDTLS_GCM_DECRYPT, (const unsigned char*)iv, 12);
	mbedtls_gcm_update(&aes, (const unsigned char*)enc_data, APDU_length, dec_data, sizeof(dec_data), &data_read);
	mbedtls_gcm_finish(&aes, NULL, 0, NULL, tag, 16);

	// Free AES-GCM context
	mbedtls_gcm_free(&aes );

	// Copy decrypted data to the output parameter
	for(size_t i = 0; i < data_read; i++){
		decrypted_data[i] = dec_data[i];
	}

	// Log the completion of MBUS frames processing
	ESP_LOGI(PROCESSOR_TAG, "Processing of MBUS frames done.");

	// Return the number of bytes read
	return data_read;
}
