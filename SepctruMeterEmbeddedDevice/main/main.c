// Include necessary libraries
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <esp_tls.h>

// Inclusion of the Spectrumeter components
#include "FrameDataExtractor.h"
#include "FrameProcessor.h"
#include "WifiHttpClient.h"

#include "cJSON.h"
#include "driver/uart.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "nvs_flash.h"

// Define constants for MBUS communication
#define MBUS_UART_NUM 	(1)
#define MBUS_RX_PIN     5
#define MBUS_TX_PIN     -1
#define MBUS_BUF_SIZE	512
#define MBUS_START_BIT  0x68
#define MBUS_END_BIT    0x16

// Define a tag for logging purposes
#define SPECTRUMETER_TAG "Spectrumeter"

// Global variables for storing JSON data and decrypted data
char json_data_to_send[400];
uint8_t decrypted_data_for_extraction[400];

// Function to read data from the smart meter through UART
void read_smartmeter_data(void *pvParameters) {
	ESP_LOGI(SPECTRUMETER_TAG, "Initializing UART configuration.");

	// Configure UART settings
    uart_config_t uart_config = {
        .baud_rate = 2400,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_EVEN,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
		.source_clk = UART_SCLK_DEFAULT,
    };

    // Install and configure UART
    uart_driver_install(MBUS_UART_NUM, MBUS_BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_param_config(MBUS_UART_NUM, &uart_config);
    uart_set_pin(MBUS_UART_NUM, MBUS_TX_PIN, MBUS_RX_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    ESP_LOGI(SPECTRUMETER_TAG, "UART configuration done.");

    // Buffer to store received MBUS frames
    uint8_t smartmeter_frame[MBUS_BUF_SIZE];

    while (1) {
    	ESP_LOGI(SPECTRUMETER_TAG, "Reading MBUS frames.");

    	// Read MBUS frames from UART
    	memset(smartmeter_frame, 0, MBUS_BUF_SIZE);
        uart_read_bytes(MBUS_UART_NUM, smartmeter_frame, MBUS_BUF_SIZE, 2500 / portTICK_PERIOD_MS);

        // Check for the start of the MBUS frame
        if(!(smartmeter_frame[0] == 0x68 && smartmeter_frame[1] == 0xFA && smartmeter_frame[2] == 0xFA && smartmeter_frame[3] == 0x68)){
        	ESP_LOGI(SPECTRUMETER_TAG, "Could not read MBUS frames.");
        	continue;
        }

        // Process MBUS frames, extract values, and send data to the server
        size_t data_length = processMbusFrames(smartmeter_frame, smartmeter_frame + 0xFA + 6, decrypted_data_for_extraction);
        extract_values_from_data(decrypted_data_for_extraction, data_length, json_data_to_send);
        send_data_to_server(json_data_to_send);

        ESP_LOGI(SPECTRUMETER_TAG, "MBUS frames processed and sent to server.");
    }
}

// Main application entry point
void app_main(void)
{
	ESP_LOGI(SPECTRUMETER_TAG, "Starting device");

	// Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // Initialize Wi-Fi in station mode
    wifi_init_sta();

    ESP_LOGI(SPECTRUMETER_TAG, "ESP wifi mode station initialized");

    // Create a task to read data from the smart meter
    xTaskCreate(read_smartmeter_data, "ReadSmartmeterData", 4096, NULL, 1, NULL);
}

